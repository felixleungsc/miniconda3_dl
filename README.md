## Docker image for deep learning
[![Docker build](https://img.shields.io/docker/automated/jrottenberg/ffmpeg.svg)](https://hub.docker.com/r/felixleung/miniconda3_dl/)

The image is based on continuumio/miniconda3, with the conda environment built using [dl_env_linux.yml](https://github.com/udacity/deep-learning/blob/master/environments/dl_env_linux.yml) from Udacity's deep-learning repo.

To start a Jupyter Notebook:
```
$ docker run -it -v $PWD:/opt/nb -p 8888:8888 felixleung/miniconda3_dl \
/bin/bash -c "source activate dl && mkdir -p /opt/nb && jupyter notebook --notebook-dir=/opt/nb --ip='0.0.0.0' --port=8888 --no-browser"
```
