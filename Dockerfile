FROM continuumio/miniconda3

RUN curl -O https://raw.githubusercontent.com/udacity/deep-learning/master/environments/dl_env_linux.yml
RUN conda env create -f dl_env_linux.yml
